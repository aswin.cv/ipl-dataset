from import_csv_into_mysql import import_to_DBtabel
import csv
import matplotlib.pyplot as plt
from mysql.connector import MySQLConnection, Error
import mysql
import time


def get_DB_connection():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='ipldata',
                                       user='root', password='')
    except Error as e:
        print(e)

    return conn


def top_ecomical_bowler(conn):
    bowlers = []
    economic_rate = []
    cursor = conn.cursor()
    cursor.execute("select bowler,\
                    ROUND((sum(total_runs)-sum(bye_runs)-sum(legbye_runs))/ \
                    ROUND(COUNT(over) / 6, 0),2 )as run from deliveries where \
                    deliveries.match_id in (select id from matches where\
                    season= 2015) group by bowler order by run limit 5")         
    sql_rows = cursor.fetchall()
    print('Total Row(s):', cursor.rowcount)
    for row in sql_rows:
        bowler = row[0]
        economy = float(row[1])
        economic_rate.append(economy)
        bowlers.append(bowler)
    print(bowlers, economic_rate)
    plot_graph(bowlers, economic_rate)


def plot_graph(bowler, econmy):
    y_pos = econmy
    x_pos = bowler
    plt.plot(x_pos, y_pos, color='green', marker='s', markerfacecolor='red')
    plt.ylabel('Economy Rate')
    plt.xlabel('Bowlers Name')
    plt.title('Matches Played per year')
    plt.show()


if __name__ == "__main__":
    start_time = time.time()
    filename = "matches.csv"
    connection = get_DB_connection()
    top_ecomical_bowler(connection)
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)