from import_csv_into_mysql import import_to_DBtabel
import csv
import matplotlib.pyplot as plt
from mysql.connector import MySQLConnection, Error
import mysql


def my_timer(orig_func):
    import time

    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        print('{} ran in: {} sec'.format(orig_func.__name__, t2))
        return result

    return wrapper


@my_timer
def get_DB_connection():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='ipldata',
                                       user='root', password='')
    except Error as e:
        print(e)
    total_match_played(conn)


def total_match_played(conn):
    total_match_played = {}
    years = []
    cursor = conn.cursor()
    cursor.execute("SELECT count(id),season FROM matches group by season")
    sql_rows = cursor.fetchall()
    print('Total Row(s):', cursor.rowcount)
    for row in sql_rows:
        total_match_played[row[1]] = row[0]
        years.append(row[0])
    print(total_match_played, years)
    plot_graph(total_match_played, years)


def plot_graph(total_match_played, years):
    y_pos = []
    x_pos = []
    for each_year in total_match_played:
        y_pos.append(total_match_played[each_year])
        x_pos.append(each_year)
    plt.bar(x_pos, y_pos, align='center', alpha=.8)
    plt.xticks(x_pos, years)
    plt.ylabel('Match Played')
    plt.title('Matches Played per year')
    plt.show()


if __name__ == "__main__":
    filename = "matches.csv"
    get_DB_connection()