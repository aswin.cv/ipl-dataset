import csv
import matplotlib.pyplot as plt
import time


def get_match_id(filename, year):
    match_id = []
    with open(filename) as file:
        match_data = csv.DictReader(file)
        for each in match_data:
            if each['season'] == '2016':
                match_id.append(int(each['id']))
    return match_id


def get_teams_extra_run(filename, match_id):
    extra_runs = {}
    with open(filename) as file:
        match_data = csv.DictReader(file)
        for each in match_data:
            if int(each['match_id']) in match_id:
                if each['bowling_team'] not in extra_runs:
                    extra_runs[each['bowling_team']] = int(each['extra_runs'])
                else:
                    extra_runs[each['bowling_team']] += int(each['extra_runs'])
    return extra_runs


def plot_graph(data):
    plt.bar([key for key in data.keys()], [value for value in data.values()],
            align='center', alpha=.8)
    plt.xticks([key for key in data.keys()], fontsize=6)
    plt.ylabel('Runs')
    plt.xlabel('teams')
    plt.title('Extra runs conceded by teams')
    plt.show()


if __name__ == "__main__":
    start_time = time.time()
    filename_1 = "matches.csv"
    filename_2 = "deliveries.csv"
    year = 2016
    match_id = get_match_id(filename_1, year)
    teams_extra_runs = get_teams_extra_run(filename_2, match_id)
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)
    plot_graph(teams_extra_runs)
