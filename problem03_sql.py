from import_csv_into_mysql import import_to_DBtabel
import csv
import matplotlib.pyplot as plt
from mysql.connector import MySQLConnection, Error
import mysql


def get_DB_connection():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='ipldata',
                                       user='root', password='')
    except Error as e:
        print(e)

    return conn


def extra_runs(conn):
    extra_runs = {}
    years = []
    cursor = conn.cursor()
    cursor.execute("select bowling_team,sum(extra_runs) from deliveries as d \
                    where d.match_id in (select id from matches  \
                    where season= 2016) group by bowling_team")
    sql_rows = cursor.fetchall()
    print('Total Row(s):', cursor.rowcount)
    for row in sql_rows:
        extra_runs[row[0]] = int(row[1])
       
    print(extra_runs, years)
    plot_graph(extra_runs)


def plot_graph(teams_extra_runs):
    print(teams_extra_runs)
    y_pos = []
    x_pos = []
    for team_name in teams_extra_runs:
        y_pos.append(teams_extra_runs[team_name])
        x_pos.append(team_name)
    plt.bar(x_pos, y_pos, align='center', alpha=.8)
    plt.xticks(x_pos, x_pos, fontsize=5.8)
    plt.ylabel('Runs')
    plt.xlabel('teams')
    plt.title('Extra runs conceded by teams')
    plt.show()


if __name__ == "__main__":
    filename = "matches.csv"
    connection = get_DB_connection()
    extra_runs(connection)