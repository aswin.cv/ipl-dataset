import csv
import matplotlib.pyplot as plt


def total_match_played(filename):
    years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
    total_match_played = {}
    with open(filename) as file:
        match_data = csv.DictReader(file)
        for each in match_data:
            for year in years:
                if year not in total_match_played:
                    total_match_played[year] = 0
                elif int(each['season']) == year:
                    total_match_played[year] += 1

        print(total_match_played, years)
    plot_graph(total_match_played, years)


def plot_graph(total_match_played, years):
    y_pos = []
    x_pos = []
    for each_year in total_match_played:
       # print(each_year, total_match_played[each_year])
        y_pos.append(total_match_played[each_year])
        x_pos.append(each_year)
    plt.bar(x_pos, y_pos, align='center', alpha=.8)
    plt.xticks(x_pos, years)
    plt.ylabel('Match Played')
    plt.title('Matches Played per year')
    plt.show()


if __name__ == "__main__":
    filename = "matches.csv"
    total_match_played(filename)
