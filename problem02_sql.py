from import_csv_into_mysql import import_to_DBtabel
import csv
import matplotlib.pyplot as plt
from mysql.connector import MySQLConnection, Error
import mysql


def get_DB_connection():
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='ipldata',
                                       user='root', password='')
    except Error as e:
        print(e)

    return conn


def teams_win(conn):
    total_wins_per_year = {}
    years = []
    cursor = conn.cursor()
    cursor.execute("select season, winner, count(winner) from matches group by \
                    season, winner")
    sql_rows = cursor.fetchall()
    print('Total Row(s):', cursor.rowcount)
    years = set()
    teams = set()
    for row in sql_rows:
        if row[0] not in total_wins_per_year:
            total_wins_per_year[row[0]] = {}
            total_wins_per_year[row[0]][row[1]] = row[2]
        else:
            total_wins_per_year[row[0]][row[1]] = row[2]
        years.add(row[0])
        if row[1] != '':
            teams.add(row[1])
    print(total_wins_per_year)
    print("teams", teams)
    total_wins_per_year = get_teams_win_data(total_wins_per_year, years, teams)
    print(total_wins_per_year)
    plot_graph(total_wins_per_year, years, teams)

    
def get_teams_win_data(teams_win_per_year, years, teams):
    teams_win_data = {}
    for year in years:
        temp = 0
        for team in teams:
            if team not in teams_win_data:
                if team in teams_win_per_year[year]:
                    teams_win_data[team] = list()
                    teams_win_data[team].append(
                        teams_win_per_year[year][team]+temp)
                    temp += teams_win_per_year[year][team]
                else:
                    teams_win_data[team] = list()
                    teams_win_data[team].append(0)
            else:
                if team in teams_win_per_year[year]:
                    temp += teams_win_per_year[year][team]
                    teams_win_data[team].append(temp)
                else:
                    teams_win_data[team].append(0)
    return teams_win_data


def plot_graph(y_pos, years, teams):
    x_pos = list(years)
    teams = list(teams)
    colors = ['silver', 'brown', 'black', 'coral', 'saddlebrown', 'orange',
              'yellow', 'darkolivegreen', 'darkseagreen', 'lightgreen',
              'blue', 'indigo', 'violet', 'purple']
    i = 0
    team_colors = {}
    for each in teams:
        team_colors[each] = colors[i]
        i += 1
    teams = list(teams)
    for team in reversed(teams):
        plt.bar(x_pos, y_pos[team],
                width=0.45, color=team_colors[team])
    for each in y_pos:
        plt.plot([], [], color=team_colors[each], label=each, linewidth=3)
    plt.legend(fontsize=7, loc=1)
    plt.ylabel('Number of wins')
    plt.title('Wins per year ')
    plt.show()


if __name__ == "__main__":
    filename = "matches.csv"
    connection = get_DB_connection()
    teams_win(connection)