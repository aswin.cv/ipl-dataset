
import csv
import matplotlib.pyplot as plt
import time


def csv_data(filename, years, teams):
    teams_win_per_year = {}
    with open(filename) as file:
        match_data = csv.DictReader(file)
        for each in match_data:
            year = int(each['season'])
            if year not in teams_win_per_year:
                teams_win_per_year[year] = {}
            team = each['winner']
            if team == 'Rising Pune Supergiant':
                team = 'Rising Pune Supergiants'
            if team not in teams_win_per_year[year]:
                teams_win_per_year[year][team] = 1
            else:
                teams_win_per_year[year][team] = int(
                    teams_win_per_year[year][team])+1
            years.add(year)
            if team != '':
                teams.add(team)
    # print(years)
    years = sorted(years)
    teams_win_data =\
        get_teams_win_data(teams_win_per_year, years, teams)
    plot_graph(teams_win_data, years, teams)


def get_teams_win_data(teams_win_per_year, years, teams):
    teams_win_data = {}
    for year in years:
        temp = 0
        for team in teams:
            if team not in teams_win_data:
                if team in teams_win_per_year[year]:
                    teams_win_data[team] = list()
                    teams_win_data[team].append(
                        teams_win_per_year[year][team]+temp)
                    temp += teams_win_per_year[year][team]
                else:
                    teams_win_data[team] = list()
                    teams_win_data[team].append(0)
            else:
                if team in teams_win_per_year[year]:
                    temp += teams_win_per_year[year][team]
                    teams_win_data[team].append(temp)
                else:
                    teams_win_data[team].append(0)
    return teams_win_data


def plot_graph(teams_win_data, years, teams):
    print(teams_win_data)
    x_pos = years
    legends = set()
    teams = list(teams)
    colors = ['silver', 'brown', 'black', 'coral', 'saddlebrown', 'orange',
              'yellow', 'darkolivegreen', 'darkseagreen', 'lightgreen',
              'blue', 'indigo', 'violet', 'purple']
    i = 0
    team_colors = {}
    for each in teams:
        team_colors[each] = colors[i]
        i += 1
    i = 0
    print(teams)
    p1 = plt.bar(x_pos, teams_win_data[teams[0]], width=.45,
                 color=team_colors[teams[0]])
    legends.add(p1[0])
    prev = teams_win_data[teams[0]]
    teams = list(teams_win_data.keys())
    print(teams)
    i += 1
    for y_pos in reversed(teams):
        plt.bar(x_pos, teams_win_data[y_pos],
                width=0.45, color=team_colors[y_pos])
        i += 1
        prev = teams_win_data[y_pos]

    for each in teams_win_data:
        print(each)
        plt.plot([], [], color=team_colors[each], label=each, linewidth=3)
    plt.legend(fontsize=7, loc=1)
    plt.ylabel('Number of wins')
    plt.title('Wins per year ')
    plt.xticks(x_pos, x_pos)
    plt.show()


if __name__ == "__main__":
    start_time = time.time()
    years = set()
    teams = set()
    filename = "matches.csv"
    csv_data(filename, years, teams)
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)
