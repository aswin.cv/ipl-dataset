import csv
import matplotlib.pyplot as plt


def get_match_id(filename, year):
    match_id = []
    with open(filename) as file:
        match_data = csv.DictReader(file)
        for each in match_data:
            if int(each['season']) == year:
                match_id.append(int(each['id']))
    return match_id


def get_bowlers_economy(filename, match_id):
    bowler_over = {}
    bowler_runs = {}
    bowlers_economy = {}
    with open(filename) as file:
        match_data = csv.DictReader(file)
        over = 0
        for each in match_data:
            if int(each['match_id']) in match_id:
                if each['bowler'] not in bowler_over:
                    bowler_over[each['bowler']] = 1
                    bowler_runs[each['bowler']] = int(each['total_runs']) -\
                    int(each['legbye_runs'])-int(each['bye_runs'])
                    bowlers_economy[each['bowler']] = bowler_runs[each['bowler'\
                    ]]
                    bowler_over[each['bowler']]
                    over = each['over']
                else:
                    if over != each['over']:
                        bowler_over[each['bowler']] += 1
                        over = each['over']
                    bowler_runs[each['bowler']] += int(each['total_runs'])-int(
                        each['legbye_runs'])-int(each['bye_runs'])
                    bowlers_economy[each['bowler']] = bowler_runs[each['bowler']
                                                               ]/bowler_over[each['bowler']]
    bowlers_economy = sorted(
        bowlers_economy.items(), key=lambda kv: kv[1])
    print(bowlers_economy[0:5])
    return bowlers_economy[0:5]


def plot_graph(top_bowler):
    y_pos = []
    x_pos = []
    for bowler in top_bowler:
        y_pos.append(bowler[1])
        x_pos.append(bowler[0])
    plt.plot(x_pos, y_pos, color='green', marker='s', markerfacecolor='red')
    plt.ylabel('Economy Rate')
    plt.xlabel('Bowlers Name')
    plt.title('Matches Played per year')
    plt.show()


if __name__ == "__main__":
    filename_1 = "matches.csv"
    filename_2 = "deliveries.csv"
    year = 2015
    match_id = get_match_id(filename_1, year)
    bowlers_economy = get_bowlers_economy(filename_2, match_id)
    # top_economical_bowler = sorted(
    #     bowlers_economy.items(), key=lambda kv: kv[1])[0:5]
    print(bowlers_economy)
    plot_graph(bowlers_economy)
