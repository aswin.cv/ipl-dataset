import csv
import matplotlib.pyplot as plt


def get_batsmen_runs(filename):
    runs = {}
    with open(filename) as file:
        match_data = csv.DictReader(file)
        for each in match_data:
            if each['batsman'] not in runs:
                runs[each['batsman']] = int(each['batsman_runs'])
            else:
                runs[each['batsman']] += int(each['batsman_runs'])
    top_batsman = (sorted(
        runs.items(), key=lambda kv: kv[1]))[len(runs)-5:len(runs)]

    print(top_batsman)
    plot_graph(top_batsman)


def plot_graph(top_batsman):
    y_pos = []
    x_pos = []
    for bowler in top_batsman:
        y_pos.append(bowler[1])
        x_pos.append(bowler[0])
    plt.plot(x_pos, y_pos, color='green', marker='s', markerfacecolor='red')
    plt.ylabel('Batsman Runs')
    plt.xlabel('Batsman Name')
    plt.title('Top Run getters of all season')
    plt.show()


if __name__ == "__main__":
    filename = "deliveries.csv"
    get_batsmen_runs(filename)
